tree grammar TExpr3;

options {
  tokenVocab=Expr;

  ASTLabelType=CommonTree;

  output=template;
  superClass = TreeParserTmpl;
}

@header {
package tb.antlr.kompilator;
}

@members {
  Integer numer = 0;
  String varAndDepth = "";
  Integer loopNumber = 0;
}
prog    : (e+=zakres | e+=expr | d+=decl)* -> program(name={$e},deklaracje={$d})
;

zakres : ^(BEGIN {enterScope();} (e+=zakres | e+=expr | d+=decl)*{leaveScope();}) -> blok(wyr={$e},dekl={$d})
;

decl  :
        ^(VAR i1=ID) {varAndDepth=$ID.text.concat( addLocalVariable($ID.text));} -> dek(n={varAndDepth})
    ;
    catch [RuntimeException ex] {errorID(ex,$i1);}

expr    : ^(PLUS  e1=expr e2=expr) -> dodaj(p1={$e1.st},p2={$e2.st})
        | ^(MINUS e1=expr e2=expr) -> odejmij(p1={$e1.st},p2={$e2.st})
        | ^(MUL   e1=expr e2=expr) -> pomnoz(p1={$e1.st},p2={$e2.st})
        | ^(DIV   e1=expr e2=expr) -> podziel(p1={$e1.st},p2={$e2.st})
        | ^(PODST i1=ID   e2=expr) {varAndDepth=$ID.text.concat( getLocalVariable($ID.text));} -> zapisz(i1={varAndDepth}, p1={$e2.st})
        | INT  {numer++;}          -> int(i={$INT.text},j={numer.toString()})
        | ID                       {varAndDepth=$ID.text.concat( getLocalVariable($ID.text));} -> id(n={varAndDepth})
        | ^(IF e1=expr e2=expr e3=expr?) {loopNumber++;} -> if(e1={$e1.st}, e2={$e2.st}, e3={$e3.st}, nr={loopNumber.toString()})
    ;
    
    