package tb.antlr.interpreter;

import java.util.HashMap;
import java.util.Map;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

public class MyTreeParser extends TreeParser {
	public Map<String, Integer> variables = new HashMap<String, Integer>();
    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' '));
    }
    
    protected void varAdd(String text) {
    	variables.put(text, 1);
    }
    
    protected void varPut(String text, String number) {
    	variables.replace(text, Integer.parseInt(number));
    }
    
    protected Integer varGet(String text) {
    	return variables.get(text);
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
}
